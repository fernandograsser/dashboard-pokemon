# Trabalho de Computação em Estatística 2

## Alunos
- Ana Haidar - 190024143
- Eduardo Côrtes - 170140784
- Fernando Grasser - 160049881
- Lucas Moraes - 190033151
- Tiago Sampaio - 190038705
  
## Instruções
Para executar o projeto, execute `cd src/` e `python pokemon.py` (com `python3` em sistemas linux) no seu terminal, estando dentro da pasta do seu clone deste repositório.

Seu terminal exibirá um link em seu localhost, como por exemplo `http://127.x.x.1:8085`. Para visualizar o dashboard, basta acessar essa URL em seu navegador.

![alt text](<./imagens/dash1.png>) 
